" don't try to be vi compatible
set nocompatible

" searches
set hlsearch
set ignorecase
set smartcase

" for plugin to load correctly
filetype indent plugin on
" syntax highlighting
syntax on
" copy the indentation from the previous line
set autoindent
" number of space characters that will be inserted when the tab key is pressed
set tabstop=2
" number of space characters inserted for indentation
set shiftwidth=2
" insert space characters whenever the tab key is pressed
set expandtab

" switch paste mode with f2
set pastetoggle=<F2>

" colorscheme gruvbox
set background=dark
set termguicolors
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
